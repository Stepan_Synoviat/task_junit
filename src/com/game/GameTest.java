package com.game;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class GameTest {

	@InjectMocks
	Game game;

	@Mock
	MockExample mockExample;

	@org.junit.jupiter.api.BeforeEach
	void setUp() {
	}

	@org.junit.jupiter.api.AfterEach
	void tearDown() {
	}

	@org.junit.jupiter.api.Test
	void chooseFighter() {
	}

	@org.junit.jupiter.api.Test
	void findWinnerTest1() {
		Game game = new Game(mockExample);
		Fighter droid = new Droid("n1");
		Fighter robot = new Robot("n2");
		//set hp for robot = 0;
		((Robot) robot).setHp(0);
		// check who has more hp
		Fighter theWinner = game.findWinner(droid, robot);
		// compare actual and expected Fighter
		assertSame(theWinner, droid);
		System.out.println("The winner is :" + theWinner.getName());
	}

	@org.junit.jupiter.api.Test
	void findWinnerTest2() {
		Game game = new Game(mockExample);
		Fighter droid = new Droid("n1");
		Fighter robot = new Robot("n2");
		//set hp for robot = 0;
		((Robot) robot).setHp(0);
		//set hp for droid = 0;
		((Droid) droid).setHp(0);
		// check who has more hp
		System.out.println("Droid has " + droid.getHp() + " hp");
		System.out.println("Robot has " + robot.getHp() + " hp");
		Fighter theWinner = game.findWinner(droid, robot);
		// compare actual and expected Fighter
		assertNotEquals(theWinner, droid);
		System.out.println("The winner is :" + theWinner.getName());
	}


	@org.junit.jupiter.api.Test
	void askForNew() {
	}

	@org.junit.jupiter.api.Test
	void gameOnTest() {
		Fighter droid = new Droid("n1");
		Fighter med = new Medic("n2");
		//set hp for medic = 0;
		((Medic) med).setHp(0);
		//check, have we continue the battle
		boolean bol = Game.gameOn(droid, med);
		// expected false
		assertFalse(bol);
		System.out.println("the game will stop, because fighter " + med.getName() + " don`t have hp");
	}

	// test method with mockito and mockito.verify
	@Test
	public void hasMoreEnergyTest() {
		MockExample mockExample = Mockito.mock(MockExample.class);
		Game game = new Game(mockExample);
		when(mockExample.hasMoreEnergy(10, 5)).thenReturn(10);

		assertEquals(game.hasMoreEnergy(10, 5), 10);
		verify(mockExample).hasMoreEnergy(10, 5);
	}

	// test void method with mockito
	@Test
	public void newGameTest() {
		Game game = mock(Game.class);
		doNothing().when(game).newGame((isA(Fighter.class)), isA(Fighter.class));
		Fighter droid = new Droid("n1");
		Fighter med = new Medic("n2");

		game.newGame(droid, med);

		verify(game).newGame(droid, med);
	}
}

