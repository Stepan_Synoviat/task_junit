import java.util.Random;

public class Droid implements Fighter {

    private int energy;
    private int hp;
    private int maxDamage = 50;
    private int causedDamage;
    private int defense = 50;

    public Droid(int energy, int hp, int defense) {
        this.hp = hp;
        this.energy = energy;
        this.defense = defense;
        System.out.println("I am Droid. I fight with shots. You can`t beat me");
    }

    public int getRandDamage() {
        if (maxDamage <= 1) {
            throw new IllegalArgumentException("max damage must be greater than min");
        }

        Random r = new Random();
        int causedDamage = r.nextInt((maxDamage - 1) + 1) + 1;
        return causedDamage;
    }

    public void getPlayerStatus() {
        //System.out.println("Player name = " + name);
        System.out.println("Health points = " + getHp());
        System.out.println("Deffense = " + getDefense());
        System.out.println("Done damage = " + causedDamage);
    }

    public int fightEnemy(int causedDamage){
        int deffense = getDefense();
        if (causedDamage == 0) {
            System.out.println("You miss this strike");
            return hp;
        } else if (hp <= 2) {
            hp -= causedDamage * 2 - deffense;
            System.out.println("--- Attacked opponent with " + (causedDamage * 2) + " damage ---");
            return hp;
        } else {
            hp -= causedDamage - deffense;
            System.out.println("--- Attacked opponent with " + causedDamage + " damage ---");
            return hp;
        }
    }


    @Override
    public int defend(int enemyDamage) {
        setDefense(getDefense() - 10);
        int deffense = getDefense();
        deffense += enemyDamage;
        System.out.println("Now my defense = " + deffense);
        return deffense;
     }


    @Override
    public int getInjure(int enemyDamage) {
        hp -= enemyDamage;
        return hp;
    }

    @Override
    public boolean isAlive() {
        return getHp() > 0;
    }

    @Override
    public void giveUp() {
        System.out.println("I give up. You win");
    }

    @Override
    public void skipAndPray() {
        System.out.println("I refuse to have a move");
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public int getHp() {
        return hp;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }
}
