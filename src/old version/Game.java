import java.util.Scanner;

public class Game {

    public boolean gameOn(Droid d1, Droid d2) {
        return d1.isAlive() && d2.isAlive();
    }

    public int strike(Droid d1, Droid d2) {
        int dam1 = d1.getRandDamage();
        //int en1 = d1.getEnergy();
        d1.fightEnemy(dam1);
        d2.getInjure(dam1);
        //d1.setEnergy(en1 -1);

        //System.out.println("damage:" + dam1);
       // System.out.println("hp of enemy now " + d2.getHp());
        return dam1;
    }

    //int enemydam = strike(Droid d1, Droid d2);
    public int defend(Droid d, int enemydam) {
        System.out.println("I chose to defend");
        d.defend(enemydam);
        return (d.getDefense());

    }

    public static int isWinner(Droid d1, Droid d2) {
        int droidDeadIndex;
        if (d1.getHp() <= 0) {
            System.out.println();
            System.out.println("---Player #1 died!");
            System.out.println("--- Player 2 is the Winner! ---");
            System.out.println();
            droidDeadIndex = 1;

        } else if (d2.getHp() <= 0) {
            System.out.println();
            System.out.println("---Player #2 died!");
            System.out.println("--- Player 1 is the Winner ---");
            System.out.println();
            droidDeadIndex = 2;

        } else {
            System.out.println("Next Round");
            droidDeadIndex = 0;
        }
        return droidDeadIndex;

    }
}


