public interface Fighter {

    int fightEnemy(int causedDamage);

    int defend(int decreasedamage);

    int getInjure(int enemydamage);

    boolean isAlive();

    void skipAndPray();

    void giveUp();

    void getPlayerStatus();

}

