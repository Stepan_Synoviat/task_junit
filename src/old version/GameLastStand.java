public class GameLastStand {

    public void battleToend(Droid d1, Droid  d2) {
        int dam1 = d1.getRandDamage();
        d1.fightEnemy(dam1);
        d2.getInjure(dam1);

        System.out.print("damage:");
        System.out.println(dam1);
        System.out.println(d2.getHp());

        int dam2 = d2.getRandDamage();
        d2.fightEnemy(dam2);
        d1.getInjure(dam2);
        System.out.print("damage:");
        System.out.println(dam2);
        System.out.println(d1.getHp());
    }


    public void whoHaveMoreHp(Droid d1, Droid d2) {
        if (d1.getHp() > d2.getHp()) {
            System.out.println("Droid #1 won!");
        } else {
            System.out.println("Droid #2 won!");
        }
    }

    public void battleDroidtoEnd(Droid d1, Droid d2) {
        int i = d1.getEnergy();
        int k = d2.getEnergy();

        while ((d1.getEnergy()) > 0 && (d2.getEnergy() > 0)) {
            battleToend(d1, d2);
            i--;
            d1.setEnergy(i);
            k--;
            d2.setEnergy(k);
        }

        whoHaveMoreHp(d1, d2);

    }

}
